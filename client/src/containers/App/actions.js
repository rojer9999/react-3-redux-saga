import { USER_LOG_IN, USER_LOG_OUT } from './actionTypes';

export const logIn = (data) => ({
  type: USER_LOG_IN,
  payload: {
    data
  }
});

export const logOut = () => ({
  type: USER_LOG_OUT
});
