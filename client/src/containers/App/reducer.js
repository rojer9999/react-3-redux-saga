import { USER_LOG_IN, USER_LOG_OUT } from './actionTypes';

const initialState = {
  user: {
    avatar: '',
    login: '',
    _id: '',
    role: ''
  },
  auth: false
}

export default function (state = initialState, { type, payload }) {
  switch (type) {
    case USER_LOG_IN:
      const { user, auth } = payload.data;
      return {
        ...state,
        user,
        auth
      }

    case USER_LOG_OUT:
      return initialState
      
    default:
      return state;
  }
}