import React, { Component, Fragment } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import Chat from '../Chat';
import LoginForm from '../../components/LoginForm';
import Users from '../Users';
import Header from '../../components/Header';
import ErrorBoundary from '../../components/ErrorBoundary';
import Spinner from '../../components/Spinner';
import PageNotFound from '../../components/PageNotFound';
import { getHeaderData } from '../Chat/service';
import { ROUTES } from '../../constants';
import { logIn, logOut } from './actions';

class App extends Component {
  constructor(props) {
    super(props);

    this.setAuth = this.setAuth.bind(this);
    this.onLogOut = this.onLogOut.bind(this);
  }
  componentDidMount() {
    this.setAuth();
  }

  setAuth = () => {
    const avatar = localStorage.getItem('avatar');
    const login = localStorage.getItem('login');
    const _id = localStorage.getItem('_id');
    const auth = localStorage.getItem('auth');
    const role = localStorage.getItem('role');

    if (auth) {
      const userObj = {
        user: {
          avatar,
          login,
          _id,
          role
        },
        auth
      }
      this.props.logIn(userObj);
    }
  }

  onLogOut() {
    this.props.logOut();
    localStorage.clear()
  }

  render() {
    const { headerData, fetching, appError } = this.props;
    return (
      <Fragment>
        <Spinner isLoading={fetching} />
        <Router>
          <Header headerData={headerData} onLogOut={this.onLogOut} />
          <ErrorBoundary appError={appError}>
            <Switch>
              <Route exact path={ROUTES.main} render={(props) => <LoginForm {...props} setAuth={this.setAuth} />} />
              <Route exact path={ROUTES.chat} component={Chat} />
              <Route path={`${ROUTES.chat}/:id`} component={Chat} />
              <Route exact path={ROUTES.users} component={Users} />
              <Route path={`${ROUTES.users}/:id`} component={Users} />
              <Route component={PageNotFound} />
            </Switch>
          </ErrorBoundary>
        </Router>
      </Fragment>
    )
  }
}

const mapStateToProps = ({ chat, users }) => ({
  headerData: getHeaderData(chat.messages),
  users: chat.users,
  fetching: chat.fetching || users.fetching,
  appError: chat.error || users.error
});

const mapDispatchToProps = {
  logIn,
  logOut
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
