import {
  FETCH_MESSAGES,
  PUT_LIKE,
  ADD_MESSAGE,
  DELETE_MESSAGE,
  UPDATE_MESSAGE
} from './actionTypes';
import {
  onAddLike,
  createNewMessage
} from './service';

export const getMessages = (messages) => ({
  type: FETCH_MESSAGES,
  payload: {
    messages
  }
});

export const putLike = (_id) => ({
  type: PUT_LIKE,
  payload: {
    likeId: onAddLike(_id)
  }
});

export const onNewMessage = (message) => ({
  type: ADD_MESSAGE,
  payload: {
    message: createNewMessage(message)
  }
});

export const onDeleteMessage = (_id) => ({
  type: DELETE_MESSAGE,
  payload: {
    _id
  }
});

export const onUpdateMessage = (_id, message) => ({
  type: UPDATE_MESSAGE,
  payload: {
    _id,
    message
  }
});
