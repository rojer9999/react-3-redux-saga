import {
  FETCH_MESSAGES,
  FETCH_MESSAGES_SUCCESS,
  FETCH_MESSAGES_FAIL,
  PUT_LIKE,
  ADD_MESSAGE,
  ADD_MESSAGE_SUCCESS,
  ADD_MESSAGE_FAIL,
  UPDATE_MESSAGE,
  UPDATE_MESSAGE_SUCCESS,
  UPDATE_MESSAGE_FAIL,
  DELETE_MESSAGE,
  DELETE_MESSAGE_SUCCESS,
  DELETE_MESSAGE_FAIL
} from './actionTypes';

const initialState = {
  messages: [],
  fetching: false,
  error: false
};

export default function (state = initialState, action) {
  switch (action.type) {

    case DELETE_MESSAGE_FAIL:
    case UPDATE_MESSAGE_FAIL:
    case ADD_MESSAGE_FAIL:
    case FETCH_MESSAGES_FAIL:
      return {
        ...state,
        fetching: false,
        error: true
      }

    case DELETE_MESSAGE:
    case UPDATE_MESSAGE:
    case ADD_MESSAGE:
    case FETCH_MESSAGES:
      return {
        ...state,
        fetching: true
      }

    case FETCH_MESSAGES_SUCCESS:
      const { messages } = action.payload;
      return {
        ...state,
        messages,
        fetching: false,
        error: false
      }

    case ADD_MESSAGE_SUCCESS:
      return {
        ...state,
        messages: [
          ...state.messages,
          action.payload.message.data
        ],
        fetching: false,
        error: false
      }

    case PUT_LIKE:
      const { likeId } = action.payload;
      const msgs = state.messages.map(message => {
        if (message._id === likeId) {
          message.likes += 1;
        }
        return message;
      });
      
      if (!likeId) {
        return state;
      } else {
        return {
          ...state,
          messages: msgs
        }
      }

    case DELETE_MESSAGE_SUCCESS:
      return {
        ...state,
        messages: state.messages.filter(message => message._id !== action.payload._id),
        fetching: false,
        error: false
      }

    case UPDATE_MESSAGE_SUCCESS:
      const { _id, message } = action.payload;
      const updatedMessages = state.messages.map(msg => {
        if (msg._id === _id) {
          msg.message = message;
          return msg;
        } else {
          return msg;
        }
      });
      return {
        ...state,
        messages: updatedMessages,
        fetching: false,
        error: false
      }
      
    default:
      return state;
  }
}