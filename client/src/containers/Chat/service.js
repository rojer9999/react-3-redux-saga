import moment from 'moment';

const getUsers = (messages) => {
  let flags = [], output = [], l = messages.length, i;
  for (i = 0; i < l; i++) {
    if (flags[messages[i].user] || messages[i].user === 'Me'){
      continue;
    }
    flags[messages[i].user] = true;
    output.push({
      user: messages[i].user,
      avatar: messages[i].avatar
    });
  }
  return [...new Set(output)];
};

const getHeaderData = (messages) => {
  const getLastMessageAt = () => {
    let time = 0;
    messages.forEach(({ created_at }) => {
      const timestamp = new Date(created_at).getTime();
      if (time < timestamp) {
        time = timestamp;
      }
    });
    return moment(time).format("Do MM, HH:MM");
  }

  return {
    totalUsers: getUsers(messages).length,
    totalMessages: messages.length,
    lastMessageAt: getLastMessageAt()
  }
}

const lastMsgId = (messages, _id) => {
  const len = messages.length - 1;
  for (let i = len; i > 0; i--) {
    const { userId, _id: msgId } = messages[i];
    if (userId === _id) {
      return msgId;
    }
  }
}

const createNewMessage = ({ avatar, inputVal, login, _id }) => ({
  userId: _id,
  login,
  avatar,
  message: inputVal,
  likes: 0,
});

const addLikeField = (messages) => {
  const storage = JSON.parse(localStorage.getItem('likes'));
  return messages.map(message => {
    message.likes = 0;
    if (storage && storage.includes(message._id)) {
      message.likes += 1;
    }

    return message;
  });
}

const onAddLike = (_id) => {
  const storage = JSON.parse(localStorage.getItem('likes'));
  if (storage && storage.includes(_id)) {
    return false;
  }

  if (Array.isArray(storage)) {
    storage.push(_id);
    localStorage.setItem('likes', JSON.stringify(storage));
  } else {
    localStorage.setItem('likes', JSON.stringify([_id]));
  }

  return _id;
}

let divider = '';
let prevDivider = '';

const getDividerText = (message) => {
  const daysDiff = moment.utc(moment().diff(message.created_at)).format('DD') - 1;
  if (prevDivider === daysDiff) {
    return false;
  } else {
    prevDivider = daysDiff;
  }

  if (daysDiff === 1) {
    divider = 'Yesterday';
  } else {
    divider = moment(message.created_at).fromNow();
  }

  return divider;
}

export {
  getHeaderData,
  getUsers,
  getDividerText,
  addLikeField,
  onAddLike,
  createNewMessage,
  lastMsgId
}