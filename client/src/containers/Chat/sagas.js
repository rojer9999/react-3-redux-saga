import { call, put, takeEvery, all } from 'redux-saga/effects';

import HTTP from '../../http';
import { ROUTES } from '../../constants';
import {
  FETCH_MESSAGES,
  FETCH_MESSAGES_SUCCESS,
  FETCH_MESSAGES_FAIL,
  ADD_MESSAGE,
  ADD_MESSAGE_SUCCESS,
  ADD_MESSAGE_FAIL,
  UPDATE_MESSAGE,
  UPDATE_MESSAGE_SUCCESS,
  UPDATE_MESSAGE_FAIL,
  DELETE_MESSAGE,
  DELETE_MESSAGE_SUCCESS,
  DELETE_MESSAGE_FAIL
} from './actionTypes';
import { addLikeField } from './service';

export function* fetchMessages() {
  try {
    const messages = yield call(HTTP.get, ROUTES.chat);
    const messagesWithLikes = addLikeField(messages.data);
    yield put({ type: FETCH_MESSAGES_SUCCESS, payload: { messages: messagesWithLikes } });
  } catch (error) {
    yield put({ type: FETCH_MESSAGES_FAIL });
    console.log('fetchMessages error:', error.message);
  }
}

function* watchFetchMessages() {
  yield takeEvery(FETCH_MESSAGES, fetchMessages);
}

export function* addMessages({ payload }) {
  try {
    const message = yield call(HTTP.post, ROUTES.chat, payload.message);
    yield put({ type: ADD_MESSAGE_SUCCESS, payload: { message } });
  } catch (error) {
    yield put({ type: ADD_MESSAGE_FAIL });
    console.log('addMessages error:', error.message);
  }
}

function* watchAddMessages() {
  yield takeEvery(ADD_MESSAGE, addMessages);
}

export function* updateMessages({ payload }) {
  try {
    const message = yield call(HTTP.put, `${ROUTES.chat}/${payload._id}`, payload.message);
    yield put({ type: UPDATE_MESSAGE_SUCCESS, payload: { ...message.data } });
  } catch (error) {
    yield put({ type: UPDATE_MESSAGE_FAIL });
    console.log('updateMessages error:', error.message);
  }
}

function* watchUpdateMessages() {
  yield takeEvery(UPDATE_MESSAGE, updateMessages);
}

export function* deleteMessages({ payload }) {
  try {
    yield call(HTTP.delete, `${ROUTES.chat}/${payload._id}`, payload.message);
    yield put({ type: DELETE_MESSAGE_SUCCESS, payload: { ...payload } });
  } catch (error) {
    yield put({ type: DELETE_MESSAGE_FAIL });
    console.log('deleteMessages error:', error.message);
  }
}

function* watchDeleteMessages() {
  yield takeEvery(DELETE_MESSAGE, deleteMessages);
}

export default function* chatSagas() {
  yield all([
    watchFetchMessages(),
    watchAddMessages(),
    watchUpdateMessages(),
    watchDeleteMessages()
  ])
}