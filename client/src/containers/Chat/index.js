import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import Modal from '../../components/Modal';
import Message from '../../components/Message';
import MesageInput from '../../components/MesageInput';
import {
  getMessages,
  onNewMessage,
  onDeleteMessage,
  putLike,
  onUpdateMessage
} from './actions';
import './style.css';
import { lastMsgId, getDividerText } from './service';
import { ROUTES } from '../../constants';

class Chat extends Component {
  componentDidMount() {
    this.props.getMessages()
  }
  
  render() {
    const {
      messages,
      onUpdateMessage,
      onNewMessage,
      onDeleteMessage,
      putLike,
      userData
    } = this.props;

    const printMessages = () => {
      return messages.map((message) => {
        return (
          <Fragment key={message._id}>
            <div>{getDividerText(message)}</div>
            <Message
              userData={userData}
              onDeleteMessage={onDeleteMessage}
              messageObj={message}
              onAddLike={putLike}
              lastMsg={lastMsgId(messages, userData._id)}
            />
          </Fragment>
        )
      })
    }

    const ChatComponent = (
      <Fragment>
        <Modal messages={messages} onUpdateMessage={onUpdateMessage} />
        <div className="container">
          <div className="messaging">
            <div className="inbox_msg">
                <div className="mesgs">
                <div className="msg_history">
                  {printMessages()}
                </div>
                <MesageInput userData={userData} onNewMessage={onNewMessage} />
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );

    return userData.role ? ChatComponent : <Redirect to={ROUTES.main} />
  }
}

const mapStateToProps = ({ chat, app }) => ({
  messages: chat.messages,
  userData: app.user
});

const mapDispatchToProps = {
  onUpdateMessage,
  getMessages,
  onNewMessage,
  onDeleteMessage,
  putLike
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);