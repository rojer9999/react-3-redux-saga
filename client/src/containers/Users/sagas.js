import { call, put, takeEvery, all } from 'redux-saga/effects';

import HTTP from '../../http';
import { ROUTES } from '../../constants';
import {
  FETCH_USERS,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAIL,
  ADD_USER,
  ADD_USER_SUCCESS,
  ADD_USER_FAIL,
  UPDATE_USER,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAIL,
  DELETE_USER,
  DELETE_USER_SUCCESS,
  DELETE_USER_FAIL
} from './actionTypes';

export function* fetchUsers() {
  try {
    const users = yield call(HTTP.get, ROUTES.users);
    yield put({ type: FETCH_USERS_SUCCESS, payload: { users } });
  } catch (error) {
    yield put({ type: FETCH_USERS_FAIL });
    console.log('fetchUsers error:', error.message);
  }
}

function* watchFetchUsers() {
  yield takeEvery(FETCH_USERS, fetchUsers);
}

export function* addUsers({ payload }) {
  try {
    const user = yield call(HTTP.post, ROUTES.users, payload.user);
    yield put({ type: ADD_USER_SUCCESS, payload: { user } });
  } catch (error) {
    yield put({ type: ADD_USER_FAIL });
    console.log('addUsers error:', error.message);
  }
}

function* watchAddUsers() {
  yield takeEvery(ADD_USER, addUsers);
}

export function* updateUsers({ payload }) {
  try {
    const user = yield call(HTTP.put, `${ROUTES.users}/${payload._id}`, payload.user);
    yield put({ type: UPDATE_USER_SUCCESS, payload: { ...user.data } });
  } catch (error) {
    yield put({ type: UPDATE_USER_FAIL });
    console.log('updateUsers error:', error.message);
  }
}

function* watchUpdateUsers() {
  yield takeEvery(UPDATE_USER, updateUsers);
}

export function* deleteUsers({ payload }) {
  try {
    yield call(HTTP.delete, `${ROUTES.users}/${payload._id}`, payload.user);
    yield put({ type: DELETE_USER_SUCCESS, payload: { ...payload } });
  } catch (error) {
    yield put({ type: DELETE_USER_FAIL });
    console.log('deleteUsers error:', error.message);
  }
}

function* watchDeleteUsers() {
  yield takeEvery(DELETE_USER, deleteUsers);
}

export default function* usersSagas() {
  yield all([
    watchFetchUsers(),
    watchAddUsers(),
    watchUpdateUsers(),
    watchDeleteUsers()
  ])
}