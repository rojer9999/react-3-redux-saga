import {
  FETCH_USERS,
  ADD_USER,
  DELETE_USER,
  UPDATE_USER
} from './actionTypes';

export const getUsers = (users) => ({
  type: FETCH_USERS,
  payload: {
    users
  }
});

export const onNewUser = (user) => ({
  type: ADD_USER,
  payload: {
    user
  }
});

export const onDeleteUser = (_id) => ({
  type: DELETE_USER,
  payload: {
    _id
  }
});

export const onUpdateUser = (_id, user) => ({
  type: UPDATE_USER,
  payload: {
    _id,
    user
  }
});
