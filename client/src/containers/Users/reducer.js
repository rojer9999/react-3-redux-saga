import {
  FETCH_USERS,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAIL,
  ADD_USER,
  ADD_USER_SUCCESS,
  ADD_USER_FAIL,
  UPDATE_USER,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAIL,
  DELETE_USER,
  DELETE_USER_SUCCESS,
  DELETE_USER_FAIL
} from './actionTypes';

const initialState = {
  users: [],
  fetching: false,
  error: false
};

export default function (state = initialState, action) {
  switch (action.type) {

    case DELETE_USER_FAIL:
    case UPDATE_USER_FAIL:
    case ADD_USER_FAIL:
    case FETCH_USERS_FAIL:
      return {
        ...state,
        fetching: false,
        error: true
      }

    case DELETE_USER:
    case UPDATE_USER:
    case ADD_USER:
    case FETCH_USERS:
      return {
        ...state,
        fetching: true
      }

    case FETCH_USERS_SUCCESS:
      const { users } = action.payload;
      return {
        ...state,
        users: users.data,
        fetching: false,
        error: false
      }

    case ADD_USER_SUCCESS:
      return {
        ...state,
        users: [
          ...state.users,
          action.payload.user.data
        ],
        fetching: false,
        error: false
      }

    case DELETE_USER_SUCCESS:
      return {
        ...state,
        users: state.users.filter(user => user._id !== action.payload._id),
        fetching: false,
        error: false
      }

    case UPDATE_USER_SUCCESS:
      const _id = action.payload._id;
      const updatedMessages = state.users.map(usr => {
        if (usr._id === _id) {
          usr = action.payload;
          return usr;
        } else {
          return usr;
        }
      });
      return {
        ...state,
        users: updatedMessages,
        fetching: false,
        error: false
      }
      
    default:
      return state;
  }
}