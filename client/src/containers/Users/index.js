import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import './style.css';
import * as actions from './actions';
import EditUser from '../../components/EditUser';
import { ROUTES } from '../../constants';

class Users extends Component {
  constructor(props) {
    super(props);

    this.onDelete = this.onDelete.bind(this);
    this.onEdit = this.onEdit.bind(this);
  }

  componentDidMount() {
    this.props.getUsers();
  }

  onDelete(id) {
    this.props.onDeleteUser(id)
  }

  onEdit(id) {
    this.props.history.push(`${ROUTES.users}/${id}`);
  }

  render() {
    const { users, match, onUpdateUser, history, user, onNewUser } = this.props;

    const printUsers = () => users.map(({ login, avatar, password, _id }) => (
      <div key={avatar} className="chat_list">
        <div className="chat_people">
          <div className="chat_img">
            <img src={avatar} alt={login}/>
          </div>
          <div className="chat_ib">
            <h5>{login}</h5>
          </div>
        </div>
        <div className="settings">
          <button onClick={() => this.onEdit(_id, { login, password, avatar })}>Edit</button>
          <button onClick={() => this.onDelete(_id)}>Delete</button>
        </div>
      </div>
    ));

    const UsersComponent = (
      <div className="container">
        <div className="messaging">
          <div className="inbox_msg">
            <div className="inbox_people">
              <div className="headind_srch">
                <div className="recent_heading">
                  <h4>Users</h4>
                </div>
                <div className="recent_heading">
                  <button onClick={() => history.push(ROUTES.newUser)} className="new-user">Add New User</button>
                </div>
              </div>
              <div className="inbox_chat">
                {printUsers()}
              </div>
            </div>
          </div>
        </div>
      </div>
    );

    if (user.role === 'admin') {
      return match.params.id ?
        <EditUser
          history={history}
          id={match.params.id}
          users={users}
          onNewUser={onNewUser}
          onUpdateUser={onUpdateUser} /> :
        UsersComponent;
    } else {
      return <Redirect to={ROUTES.chat} />
    }

  }
};

const mapStateToProps = ({ users, app }) => ({
  users: users.users,
  user: app.user
});

const mapDispatchToProps = { ...actions }

export default connect(mapStateToProps, mapDispatchToProps)(Users);