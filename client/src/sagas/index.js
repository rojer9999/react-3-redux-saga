import { all } from 'redux-saga/effects';
import chatSagas from '../containers/Chat/sagas'
import usersSagas from '../containers/Users/sagas'

export default function* rootSaga() {
  yield all([
    chatSagas(),
    usersSagas()
  ])
}