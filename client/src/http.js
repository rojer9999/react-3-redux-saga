import axios from 'axios';
import { URI } from './constants';

const HTTP = axios.create({
  baseURL: URI,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },
});

export default HTTP;