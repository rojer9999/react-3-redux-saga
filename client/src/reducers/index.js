import { combineReducers } from 'redux';

import chat from '../containers/Chat/reducer';
import app from '../containers/App/reducer';
import users from '../containers/Users/reducer';

const rootReducer = combineReducers({
  chat,
  app,
  users
})

export default rootReducer;