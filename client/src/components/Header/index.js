import React from 'react';
import { Link } from 'react-router-dom'

import './style.css';
import { ROUTES } from '../../constants';

const Header = ({ headerData, onLogOut }) => {
  const { totalUsers, totalMessages, lastMessageAt } = headerData;

  return (
    <div className="header">
      <div className="container">
        <div className="inner-container">
          <div>
            <Link to={ROUTES.chat}>
              <div className="logo"></div>
              <h1>React-Chat</h1>
            </Link>
          </div>
          <div className="data">
            <div className="inner-data">
              <div>{totalUsers} Users</div>
              <div>{totalMessages} Messages</div>
              <div>Last Message At {lastMessageAt}</div>
            </div>
            <div className="data-nav">
              <Link to={ROUTES.chat}>Chat</Link>
              <Link to={ROUTES.users}>Users</Link>
              <Link onClick={onLogOut} to={ROUTES.main}>Log out</Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;