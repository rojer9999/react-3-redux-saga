import React, { Component } from 'react';

import './style.css';
import { ROUTES } from '../../constants';

class EditUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      login: '',
      password: '',
      avatar: '',
    };
    this.onChange = this.onChange.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }

  componentDidMount() {
    const { id, users } = this.props;
    users.forEach(({ _id, login, password, avatar }) => {
      if (_id === id) {
        this.setState({ login, password, avatar })
      }
    });
  }

  onChange({ target }) {
    this.setState({
      [target.name]: target.value
    })
  }

  onSave() {
    const { onNewUser, onUpdateUser, id, history } = this.props;
    const { login, password } = this.state;
    if (ROUTES.newUser.includes(id)) {
      onNewUser({ login, password });
    } else {
      onUpdateUser(id, this.state);
    }
    history.go(-1);
  }

  onCancel() {
    this.props.history.go(-1);
  }

  render() {
    const { login, password, avatar } = this.state;
    const isNewUser = ROUTES.newUser.includes(this.props.id);
    const avatarInp = [
      <label key="htmlFor" htmlFor="avatar">Avatar</label>,
      <input
        key="placeholder"
        onChange={this.onChange}
        placeholder="avatar"
        type="text"
        value={avatar}
        name="avatar"
        className="avatar" />
    ];
  
    return (
      <div className="container edit-user">
        <label htmlFor="login">Login</label>
        <input
          onChange={this.onChange}
          placeholder="login"
          type="text"
          value={login}
          name="login"
          className="login" />
        <label htmlFor="password">Password</label>
        <input
          onChange={this.onChange}
          placeholder="password"
          type="text"
          value={password}
          name="password"
          className="password" />
          {!isNewUser && avatarInp}
          <div className="btns-block">
            <button onClick={this.onCancel}>Cancel</button>
            <button onClick={this.onSave}>Save</button>
          </div>
      </div>
    )
  } 
}

export default EditUser;