import React from 'react';
import './style.css';

const Spinner = ({ isLoading }) => {
  const spinner = (
    <div className="spinner">
      <div />
    </div>
  );

  return isLoading ? spinner : null;
};

export default Spinner;