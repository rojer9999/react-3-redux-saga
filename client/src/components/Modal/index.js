import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';

import './style.css';
import { getMessageById } from './service'

const Modal = ({ match, onUpdateMessage, history, messages }) => {
  const [ inputVal, setInputVal ] = useState('');
  const onChange = (e) => setInputVal(e.target.value);
  const currentMessage = getMessageById(messages, match.params.id);
  const onSave = () => {
    onUpdateMessage(match.params.id, {
      ...currentMessage,
      message: inputVal
    });
    history.goBack();
  }

  useEffect(() => {
    setInputVal(currentMessage.message)
  }, [currentMessage.message])

  const modal = (
    <div id="main-modal" className="overlay">
      <div className="popup">
        <h2>Edit</h2>
        <div className="close" onClick={history.goBack}>×</div>
        <div className="content">
          <input
            onChange={onChange}
            value={inputVal}
            className="input-modal" />
          <div>
            <button onClick={history.goBack}>Cancel</button>
            <button onClick={onSave}><i className="fa fa-save"></i></button>
          </div>
        </div>
      </div>
    </div>
  );

  return match.params.id ? modal : null;
};

export default withRouter(Modal);