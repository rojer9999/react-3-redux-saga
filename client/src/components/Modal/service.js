const getMessageById = (messages, _id) => {
  const msg = messages.find((message) => message._id === _id);
  if (msg && msg.message) {
    return msg
  } else {
    return ''
  }
}

export {
  getMessageById
}