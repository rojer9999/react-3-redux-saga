import React from 'react';
import './style.css';
import moment from 'moment';
import { withRouter } from 'react-router-dom';

import './style.css';
import { ROUTES } from '../../constants';

const Message = ({
  messageObj,
  onDeleteMessage,
  onAddLike,
  lastMsg,
  history,
  userData
}) => {
  const { avatar, created_at, likes, _id, message, login, userId } = messageObj;
  const time = moment(created_at).format("Do MM, HH:MM");

  const printIncoming = () => (
    <div className="incoming_msg">
      <div className="incoming_msg_img">
        <img src={avatar} alt={login}/>
      </div>
      <div className="received_msg">
        <div className="received_withd_msg">
          <p>{message}</p>
          <p className="footer_msg">
            {likes}
            <i className="fa fa-thumbs-up" onClick={() => onAddLike(_id)}></i>
          </p>
          <span className="time_date">{time}</span></div>
      </div>
    </div>
  );

  const printOutgoingMessage = () => (
    <div className="outgoing_msg">
      <div className="sent_msg">
        <p>
          {message}
          <span className="footer_msg">
            {
              lastMsg === _id &&
              <i className="fa fa-cog" onClick={() => history.push(`${ROUTES.chat}/${_id}`)} />
            }
            <i className="fa fa-trash" onClick={() => onDeleteMessage(_id)}></i>
            {likes}
            <i className="fa fa-thumbs-up"></i>
          </span>
        </p>
        <span className="time_date">{time}</span> </div>
    </div>
  )

  if (userData._id === userId) {
    return printOutgoingMessage();
  } else {
    return printIncoming();
  }
};

export default withRouter(Message);