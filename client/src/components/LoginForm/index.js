import React, { useState } from 'react';

import './style.css';
import HTTP from '../../http';
import { ROUTES } from '../../constants';

const LoginForm = ({ history, setAuth }) => {
  const initialInpState = {
    login: '',
    password: ''
  };
  const [inpValues, setValue] = useState(initialInpState);
  const [authFail, toggleAuthFail] = useState(false);

  const onChange = ({ target }) => setValue({
    ...inpValues,
    [target.name]: target.value
  });

  const onToggleAuthFail = (val) => toggleAuthFail(val);

  const onSend = (ev) => {
    ev.preventDefault();
    const { login, password } = inpValues;
    if (!login || !password) {
      return false;
    }
    
    HTTP.post('/', { login, password })
      .then(({ data }) => {
        onToggleAuthFail(false);
        const { token, avatar, login, role, _id, auth } = data
        localStorage.setItem('jwt', token);
        localStorage.setItem('avatar', avatar);
        localStorage.setItem('login', login);
        localStorage.setItem('_id', _id);
        localStorage.setItem('auth', auth);
        localStorage.setItem('role', role);

        setAuth();
        HTTP.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        history.push(role === 'admin' ? ROUTES.users : ROUTES.chat);
      })
      .catch(err => {
        if (err.response && err.response.status === 401) {
          onToggleAuthFail(true);
        } else {
          console.log(err);
        }
      });
    setValue(initialInpState);
  }

  return (
    <div className="login-form card">
      <article className="login-form card-body" >
        <h4 className="login-form card-title text-center mb-4 mt-1">Sign in</h4>
        <hr/>
        <p className="text-danger text-center">{authFail && 'Wrong login/password'}</p>
        <form>
        <div className="form-group">
        <div className="input-group">
          <div className="input-group-prepend">
            <span className="input-group-text"> <i className="fa fa-user"></i> </span>
          </div>
          <input
            className="form-control"
            placeholder="Login"
            name="login"
            type="text"
            value={inpValues.login}
            onChange={onChange} />
        </div>
        </div>
        <div className="form-group">
        <div className="input-group">
          <div className="input-group-prepend">
            <span className="input-group-text"> <i className="fa fa-lock"></i> </span>
          </div>
          <input
            className="form-control"
            placeholder="******"
            type="password"
            name="password"
            value={inpValues.password}
            onChange={onChange} />
        </div>
        </div>
        <div className="form-group">
        <button onClick={onSend} className="btn btn-primary btn-block">Login</button>
        </div>
        </form>
      </article>
    </div>
  )
}

export default LoginForm
