import React, { Component } from 'react';

class MesageInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputVal: ''
    }

    this.onSend = this.onSend.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
  }

  onInputChange(e) {
    e.preventDefault();
    this.setState({ inputVal: e.target.value });
  }

  onSend() {
    const { inputVal } = this.state;
    if (!inputVal.trim()) {
      return false;
    }

    this.props.onNewMessage({ inputVal, ...this.props.userData });
    this.setState(() => ({ inputVal: '' }));
  }

  render() {  
    return (
      <div className="type_msg">
        <div className="input_msg_write">
          <input
            value={this.state.inputVal}
            onChange={this.onInputChange}
            type="text"
            className="write_msg"
            placeholder="Type a message"
          />
          <button className="msg_send_btn" type="button" onClick={this.onSend}>
            <i className="fa fa-paper-plane-o" aria-hidden="true"></i>
          </button>
        </div>
      </div>
    );
  }
}

export default MesageInput;