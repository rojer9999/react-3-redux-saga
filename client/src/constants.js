export const URI = 'http://localhost:8000';

export const ROUTES = {
  main: '/',
  chat: '/chat',
  users: '/users',
  newUser: '/users/new-user'
}
