const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const Users = require('./models/Users');

const opts = {
    secretOrKey: 'someSecret',
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
};

passport.use(new JwtStrategy(opts, async (payload, done) => {
    try {
        const user = Users.findOne({ login: payload }, (err, doc) => {
            if (doc && doc.login === payload.login) {
                return login;
            }
        })
        return user ? done(null, user) : done({ status: 401, message: 'Token is invalid.' }, null);
    } catch (err) {
        return done(err);
    }
}));