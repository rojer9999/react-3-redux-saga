const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
require('dotenv').config();

require('./passport.config');
const indexRouter = require('./routes/index');
const chatRouter = require('./routes/chat');
const usersRouter = require('./routes/users');

const PORT = process.env.PORT || 8000;
server.listen(PORT);

// app.use(express.static(path.join(__dirname, 'client/public')));
app.use(passport.initialize());
app.use(bodyParser.json());
app.use(cors());
app.options('*', cors());

app.use('/', indexRouter);
app.use('/chat', passport.authenticate('jwt', { session: false }), chatRouter);
app.use('/users', passport.authenticate('jwt', { session: false }), usersRouter);

const dbUrl = `mongodb://${process.env.DB_USER}:${process.env.DB_PASS}@ds251507.mlab.com:51507/redux-saga-chat`;
mongoose.connect(
  dbUrl,
  { useNewUrlParser: true },
  (err) => console.log('mongodb connected', err)
);
mongoose.set('useFindAndModify', false);
// let text = '';
// app.get('/text', passport.authenticate('jwt', { session:false }), function (req, res) {
//   res.send({ text });
// });

// let isRace = false;
// let isTimeCounting = false;
// const racers = {};
// let startedAt;
// let innerTimer;

// io.on('connection', socket => {
//   console.log(`A new user connected to ${'raceRoom'}`);
  
//   socket.on('newUser', ({ token }) => {
//     if (jwt.verify(token, 'someSecret')) {
//       const { login, iat } = jwt.decode(token);
//       racers[iat] = {
//         id: iat,
//         login,
//         enteredSymbols: 0,
//         socketId: socket.id
//       };
//       io.sockets.emit('showRacers', { racers });
//     }
//   })

//   if (!isRace) {
//     socket.join('raceRoom');
    
//     if (!isTimeCounting) {
//       text = txtgen.paragraph();
//       let seconds = 10; // PREPARING FOR START
//       isTimeCounting = true;
//       const tick = () => {
//         timerId = setTimeout(tick, 1000);
//         if (!seconds) {
//           isTimeCounting = false;
//           isRace = true;
//           clearTimeout(timerId);
//           startedAt = Date.now();
          
//           let sec = 10; // RACE TIME
//           const innerTick = () => {
//             innerTimerId = setTimeout(innerTick, 1000);
//             innerTimer = innerTimerId;
//             if (sec >= 0) {
//               io.sockets.emit('finishIn', { time: sec-- });
//             } else {
//               const raceTime = Date.now() - startedAt;
//               for (const key in racers) {
//                 if (racers.hasOwnProperty(key)) {
//                   const racer = racers[key];
//                   racer.raceTime = raceTime;
//                   racer.id = key;
//                   racer.ratio = (racer.enteredSymbols / raceTime) * 10 ** 20;
//                   io.in('raceRoom').emit('win', { racer });
//                 }
//               }
//               io.in('raceRoom').emit('raceFinished', { racers });
//               clearTimeout(innerTimerId);
//             }
//           }
//           let innerTimerId = setTimeout(innerTick, 1000);
//         }
//         io.sockets.emit('timeLeft', { time: seconds-- });
//       };
//       let timerId = setTimeout(tick, 1000);
//     }
//   } else {
//     socket.emit('wait', { message: 'Just wait for the next race.' });
//   }

//   socket.on('enteredSymbols', ({ token, enteredSymbols, textLength }) => {
//     if (jwt.verify(token, 'someSecret')) {
//       const { iat } = jwt.decode(token);
//       const racer = racers[iat];
//       racer.id = iat;
//       racer.enteredSymbols = enteredSymbols;
//       racer.textLength = textLength;

//       io.in('raceRoom').emit('typing', { racer });

//       if (enteredSymbols === textLength) {
//         racer.raceTime = Date.now() - startedAt;
//         racer.ratio = enteredSymbols / racer.raceTime;
//         io.in('raceRoom').emit('win', { racer });

//         let winners = 0;
//         for (const key in racers) {
//           if (racers.hasOwnProperty(key)) {
//             const { enteredSymbols, textLength } = racers[key];
//             if (enteredSymbols === textLength) {
//               winners += 1;
//             }
//           }
//         }
//         if (winners === Object.keys(racers).length) {
//           clearTimeout(innerTimer);
//           io.in('raceRoom').emit('raceFinished', { racers });
//         }
//       }
//     }
//   })

//   socket.on('joinTheRace', () => socket.join('raceRoom'));

//   socket.on('disconnect', () => {
//     socket.leave('raceRoom');
//     io.sockets.emit('disconnected', { racers, socketId: socket.id });

//     if (io.engine.clientsCount === 0) {
//       isRace = false;
//     }
//   })
// });
