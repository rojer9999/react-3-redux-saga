const router = require('express').Router();
const Users = require('../models/Users');
const { cb, getOptionsObj, getRandomAvatar } = require('./helper');

router.get('/', (req, res) => {
  Users.find({}, cb(res));
});

router.post('/', (req, res) => {
  const { body: { login, password } } = req;
  console.log('login, password', login, password)
  Users.findOne({ login }, (err, doc) => {
    if (err) {
      return res.status(400).json(err);
    }
    if (doc) {
      return res.status(409).json({});
    }
    const newMessage = new Users({
      login,
      password,
      avatar: getRandomAvatar(1, 70)
    });
  
    newMessage.save(cb(res));
  })
});

router.put('/:_id', (req, res) => {
  const { body: { login, password, avatar } } = req;
  const { params: { _id } } = req;

  Users.findByIdAndUpdate(_id, getOptionsObj({ login, password }), (err, payload) => {
    if (err) {
      return res.status(400).json(err);
    }
    return res.status(200).json({ _id, login, password, avatar });
  });
});

router.delete('/:_id', (req, res) => {
  const { params: { _id } } = req;
  Users.findOneAndDelete({ _id }, cb(res));
});

module.exports = router;