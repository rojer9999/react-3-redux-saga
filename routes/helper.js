const cb = (res, errStatus = 400, successStatus = 200) => {
  return (err, payload) => {
    if (err) {
      return res.status(errStatus).json(err);
    }
    return res.status(successStatus).json(payload);
  }
}

const getOptionsObj = (obj) => {
  const options = {};
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      const element = obj[key];
      if (element) {
        options[key] = element;
      }
    }
  }
  return options;
}

const getRandomAvatar = (min, max) => {
  const random = Math.floor(Math.random() * (max - min + 1)) + min;
  return `https://i.pravatar.cc/150?img=${random}`;
}

module.exports = {
  cb,
  getOptionsObj,
  getRandomAvatar
}