const router = require('express').Router();
const Messages = require('../models/Messages');
const { cb } = require('./helper');

router.get('/', (req, res) => {
  Messages.find({}, cb(res));
});

router.post('/', (req, res) => {
  const { body } = req;
  const { message, userId, login, avatar, likes } = body;
  const newMsg = {
    message,
    userId,
    created_at: Date.now(),
    login,
    avatar,
    likes
  }
  const newMessage = new Messages(newMsg);

  newMessage.save((err, { _id }) => {
    if (err) {
      return res.status(400).json(err);
    }
    return res.status(200).json({ ...newMsg, _id });
  });
});

router.put('/:_id', (req, res) => {
  const { body: { message } } = req;
  const { params: { _id } } = req;
  Messages.findByIdAndUpdate(_id, { message }, (err, payload) => {
    if (err) {
      return res.status(400).json(err);
    }
    return res.status(200).json({ ...payload, _id, message });
  });
});

router.delete('/:_id', (req, res) => {
  const { params: { _id } } = req;
  Messages.findOneAndDelete({ _id }, cb(res));
});

module.exports = router;