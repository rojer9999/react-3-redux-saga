const router = require('express').Router();
const jwt = require('jsonwebtoken');

const Users = require('../models/Users');

router.post('/', (req, res) => {
  const { login, password } = req.body;
  Users.findOne({ login }, (err, doc) => {
    if (err) {
      return res.send(err);
    }
    if (doc && password === doc.password) {
      const token = jwt.sign(login, 'someSecret');
      const { _id, avatar, role } = doc;
      res.status(200).json({
        auth: true,
        token,
        role,
        _id,
        login,
        avatar
      });
    } else {
      res.status(401).json({ auth: false });
    }
  })
});

module.exports = router