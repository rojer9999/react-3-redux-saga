const mongoose = require('mongoose');

const user = mongoose.Schema({
  login: {
    type: String,
    required: true,
    minlength: 5,
    trim: true
  },
  password: {
    type: String,
    required: true,
    minlength: 5,
    trim: true
  },
  avatar: {
    type: String,
  },
  role: {
    type: String,
    default: 'user'
  }
},
{ versionKey: false });

module.exports = mongoose.model('users', user);