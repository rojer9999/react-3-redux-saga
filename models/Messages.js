const mongoose = require('mongoose');

const message = mongoose.Schema({
  message: {
    type: String,
    required: true,
    minlength: 1,
    trim: true
  },
  created_at: {
    type: Number,
    required: true
  },
  userId: {
    type: String,
    required: true
  },
  avatar: {
    type: String,
    default: ''
  }
},
{ versionKey: false });

module.exports = mongoose.model('messages', message);